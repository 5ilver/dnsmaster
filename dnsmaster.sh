#!/bin/bash
action=$1
dnshost=$2
dnsip=$3
zonefile="/etc/bind/example.com.zone"
bindcmd="service bind9 reload"

function finishupdate
{
        serialline="`cat $zonefile | grep '; serial'`"
        newserialline="$((`echo $serialline | awk '{print $1}'` + 1 )) ; serial"
        sed "s/$serialline/$newserialline/" -i $zonefile
        $bindcmd
        exit 0
}

if [ "$action" == "add" ] && [ "$dnshost" != "" ] && [ "$dnsip" != "" ]; then
        if egrep "^$dnshost IN A " $zonefile ; then
                echo "An A record for $dnshost already present in $zonefile!"
                exit 1
        fi
        if ! ping -q -c1 -w5 "$dnsip"; then
                echo "Ping to $dnsip failed, cowardly refusing to add a record."
                exit 1
        fi
        echo "Adding $dnshost on $dnsip to $zonefile"
        echo "$dnshost IN A $dnsip" >> $zonefile
        finishupdate
fi


if [ "$action" == "remove" ] && [ "$dnshost" != "" ]; then
        if ! egrep "^$dnshost IN A " $zonefile ; then
                echo "No A record for $dnshost present in $zonefile!"
                exit 1
        fi
        echo "Removing $dnshost from $zonefile"
        sed "/^$dnshost\ IN\ A /d" $zonefile -i
        finishupdate
fi

echo "Usage: $0 [add <hostname> <ipaddress>]|[remove <hostname>]"
exit 2